/* DESCRIZIONE

*/

/* LIBRERIE */
#include <ESP8266WiFi.h>
#include <AM2320.h>
#include <PubSubClient.h>
/* DEFINIZIONI */
/* PIN NODEMCU rispetto ad Arduino*/
#define D0 16
#define D1 5 // I2C Bus SCL (clock)
#define D2 4 // I2C Bus SDA (data)
#define D3 0
#define D4 2 // Same as "LED_BUILTIN", but inverted logic
#define D5 14 // SPI Bus SCK (clock)
#define D6 12 // SPI Bus MISO 
#define D7 13 // SPI Bus MOSI
#define D8 15 // SPI Bus SS (CS)
#define D9 3 // RX0 (Serial console)
#define D10 1 // TX0 (Serial console)
/*___________________________*/
#define Nome_WIFI "TIM-04870817"
#define Pass_WIFI "kxpT3kMsYSe2Akcs"

#define mqtt_server "192.168.1.150"
/*#define mqtt_user "your_username" solo se necessario
  #define mqtt_password "your_password" */
#define umidita_topic "sensoreCantina/umidita"
#define temperatura_topic "sensoreCantina/temperatura"

/* VARIABILI */
AM2320 sensoreAM2320;
WiFiClient espClient;
PubSubClient client(espClient);
float temp = 0;
float umidita = 0;

/* FUNZIONI */
/* MQTT */
void mqtt_riconnetti() {
  // Loop until we're mqtt_riconnettied
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect

    if (client.connect("SensoreCantina")) {  /*se serve aggiungi autenticazione usa-> ("ESP8266Client", mqtt_user, mqtt_password)) */
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}



/* PROGRAMMA */
void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  sensoreAM2320.begin(D2, D1); //NodeMCU non ha un pin SCL e SDA specifico

  client.setServer(mqtt_server, 1883);

  Serial.begin(9600);
  Serial.println();
  WiFi.begin(Nome_WIFI, Pass_WIFI);
  Serial.print("Wifi: Connessione...");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Connesso, IP: ");
  Serial.println(WiFi.localIP());
}


void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  if (sensoreAM2320.measure()) {
    temp = sensoreAM2320.getTemperature();
    umidita = sensoreAM2320.getHumidity();
    Serial.print("Temperatura: ");
    Serial.println(temp);
    Serial.print("Umidita': ");
    Serial.println(umidita);
  }
  else {  // error has occured
    int errorCode = sensoreAM2320.getErrorCode();
    switch (errorCode) {
      case 1: Serial.println("ERR: sensore AM2320 e' offline"); break;
      case 2: Serial.println("ERR: CRC validazione fallita."); break;
    }
  }

  /*---------------------------------------------------*/
  if (!client.connected()) {
    mqtt_riconnetti();
  }
  client.loop();

  client.publish(temperatura_topic, String(temp).c_str(), true);
  client.publish(umidita_topic, String(umidita).c_str(), true);
  digitalWrite(LED_BUILTIN, LOW);
  delay(60000);
}