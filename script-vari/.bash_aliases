alias foto='raspistill -o ~/Pictures/$RANDOM.jpg -vf -hf -awb auto'
alias caffe='cat /dev/urandom | hexdump -C | grep "ca fe"'
alias tips='clear; curl -sS "https://www.commandlinefu.com/commands/browse/last-month/sort-by-votes/plaintext" | tail -n +3 | head -n 3'
alias tipsnew='clear; curl -sS "https://www.commandlinefu.com/commands/browse/plaintext" | tail -n +3 | head -n 3'
alias cls='reset'
alias info='clear; cat /etc/*RELEASE*; hostname; hostname -I; lsb_release -d'
alias aggiorna='sudo apt update; sudo apt upgrade -r; sudo apt install -y raspi-config; echo "Aggiornamento Completato"'
alias partizioni='lsblk'
alias alias='cat ~/.bash_aliases'
alias ippubblico='curl ifconfig.me'
alias meteo='curl it.wttr.in/vicenza'
alias timer='echo "Timer Attivo: INVIO per fermarlo" ; time read'
alias bin='cd ~/bin'
alias up='git add -A; git commit -am "$(date)"; git push'

# riavvia router: alias rebootlinksys="curl -u 'admin:my-super-password' 'http://192.168.1.2/setup.cgi?todo=reboot'

alias ll='ls -la'
alias ls='ls --color=auto --group-directories-first -1'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias mkdir='mkdir -pv'
alias mount='mount |column -t'
alias h='history'
alias wget='wget -c'




