#!/usr/bin/python

# prima installa le librerie adafruit
import Adafruit_CharLCD as LCD
import sys 

# configura i pin usati per il display 16x2
lcd_rs = 25
lcd_en = 24
lcd_d4 = 23
lcd_d5 = 17
lcd_d6 = 18
lcd_d7 = 22
lcd_backlight = 2
lcd_columns = 16
lcd_rows = 2

lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows, lcd_backlight)

lcd.message(sys.argv[1] + "\n" + sys.argv[2])

# utilizzo da bash 
# python nomeScript.py "testo Riga 1" "testo Riga 2"
# usa python2.7